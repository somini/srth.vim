# srht.vim

This Vim plugin provides features for interacting with some
[sourcehut](https://sourcehut.org/) services. I wrote about why I created this
plugin [here](https://williamdurand.fr/2021/03/01/introducing-srht-vim/).

## Usage

In doubt, run `:help srht`.

This plugin provides the following features per service:

- `git.sr.ht`:
  - if [vim-fugitive](https://github.com/tpope/vim-fugitive) is
    installed, this plugin will allow `:GBrowse` to open sourcehut URLs
- `paste.sr.ht`:
  - create and retrieve pastes with `:SrhtPaste [sha]` (requires `curl` and the
    [webapi-vim](https://github.com/mattn/webapi-vim) plugin)

### Configuration

- `g:srht_token_file`: path to the file containing a sourcehut personal access
  token (which is only required for commands that interact with the sourcehut
  APIs)
  - default: `~/.srht-vim`
- `g:srht_paste_default_visibility`: default paste visibility
  - default: `unlisted`
- `g:srht_paste_domain`: domain of the paste hosting service
  - default: `paste.sr.ht`
- `g:srht_git_domain`: domain of the git hosting service
  - default: `git.sr.ht`

## Installation

Install the _srht.vim_ plugin using your favorite package manager, or use Vim's
built-in package support:

```
$ mkdir -p ~/.vim/pack/willdurand/start
$ cd !$
$ git clone https://git.sr.ht/~willdurand/srht.vim
$ vim -u NONE -c "helptags srht.vim/doc" -c q
```

## Contributing

Send patches to [my public inbox](https://lists.sr.ht/~willdurand/public-inbox)
(please edit the `[PATCH]` line to include `srht.vim`: `[PATCH srht.vim]`).

## Extra credits

The `:SrhtPaste` feature is heavily inspired by the excellent
[vim-gist](https://github.com/mattn/vim-gist) written by Yasuhiro Matsumoto and
released under the BSD License (2-clause).

## License

srht.vim is released under the MIT License. See the bundled
[LICENSE](./LICENSE.md) file for details.
